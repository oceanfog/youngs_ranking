//define ng-app
var app = angular.module('youngs_ranking', ['ui.bootstrap']);

//factory 가 들어감


app.factory('factory', function(){
	var oData = {"name":"krk"};

	return{
		set:function(oVal){
			oData = oVal;
		},
		get:function(){
			return oData;
		}
	};
});

app.controller('controller_01',[
	'$scope'
	,'$http'
	,'factory'
	,function($scope, $http, factory){
		$scope.user = {"name":"krk2"};
		$scope.name ="krk3";

		$scope.gameList = [
			{"gidx":"processing"}
			
			
		];

		$scope.init = function(){
			$scope.setGameList();
		};

		$scope.setGameList = function(){

 		$http({
	      method: 'GET'
	      ,url: "./games_dao_client.php"
	      ,headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	console.log(response);
	    	$scope.gameList = response;
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    }

	}
]);